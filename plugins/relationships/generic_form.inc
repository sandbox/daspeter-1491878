<?php

/**
 * @file
 * Relationship handler to load generic forms with keyword substitution.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('Generic Form'),
  'keyword' => 'generic_form_relation',
  'description' => t('Adds a generic form which can use keyword substitution'),
  'required context' => array(
    new ctools_context_optional(t('Any Context'), 'any'),
    new ctools_context_optional(t('Any Context'), 'any'),
  ),
  'context' => 'generic_form_relation_create_context',
  'edit form' => 'generic_form_settings_form',
  'defaults' => array(
    'file' => array(
      'type' => NULL,
      'module' => NULL,
      'name' => NULL,
    ),
    'form_id' => FALSE,
    'arguments' => array(),
    'substitute' => TRUE,
  ),
);

/**
 * Load the form context.
 */
function generic_form_relation_create_context($context, $conf, $placeholders) {
  $conf['contexts'] = $context;
  return ctools_context_create('generic_form', $conf);
}

/**
 * Context settings form.
 */
function generic_form_settings_form($form, &$form_state) {
  // Ensure the ajax callback finds this file.
  // form_load_include($form_state, 'inc', 'ctools_generic_form', 'plugins/relationships/generic_form');

  $conf = &$form_state['conf'];
  $conf += $form_state['plugin']['defaults'];
  $contexts = array();
  if (!empty($form_state['contexts'])) {
    $contexts = $form_state['contexts'];
    $contexts = _generic_form_prepare_contexts($form_state['contexts']);
  }

  // Store filtered contexts.
  $form['valid_contexts'] = array(
    '#type' => 'value',
    '#value' => $contexts,
  );
  // Hide context configuration.
  $form['context']['#access'] = FALSE;

  $form['settings']['#tree'] = TRUE;

  $conf['file'] += $form_state['plugin']['defaults']['file'];
  $form['settings']['file'] = array(
    '#type' => 'fieldset',
    '#title' => t('File'),
    '#description' => t('Usage as <a href="!url">module_load_include()</a>.', array('!url' => 'http://api.drupal.org/api/drupal/includes!module.inc/function/module_load_include/7')),
    '#collapsible' => TRUE,
    '#collapsed' => !strlen(implode('', $conf['file'])),
  );
  $form['settings']['file']['type'] = array(
    '#type' => 'textfield',
    '#title' => t('Type'),
    '#description' => t("The include file's type (file extension)."),
    '#default_value' => $conf['file']['type'],
  );
  $form['settings']['file']['module'] = array(
    '#type' => 'textfield',
    '#title' => t('Module'),
    '#description' => t('The module to which the include file belongs.'),
    '#default_value' => $conf['file']['module'],
  );
  $form['settings']['file']['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#description' => t('The base file name (without the $type extension). If omitted, $module is used; i.e., resulting in "$module.$type" by default.'),
    '#default_value' => $conf['file']['name'],
  );

  $form['settings']['form_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Form id'),
    '#description' => t('The form id as used by <a href="!url">drupal_build_form()</a>.', array('!url' => 'http://api.drupal.org/api/drupal/includes!form.inc/function/drupal_build_form/7')),
    '#required' => TRUE,
    '#default_value' => $conf['form_id'],
//     '#ajax' => array(
//       'callback' => 'generic_form_settings_form_get_arguments_ajax',
//     ),
  );

  $form['settings']['arguments'] = array(
    '#type' => 'fieldset',
    '#title' => t('Arguments'),
    '#description' => t('Define form arguments. Please save and reopen the form to load all arguments.'),
    '#attributes' => array('class' => array('form-settings-form-arguments')),
  );

  // Try to load the arguments.
  if (!empty($conf['form_id']) || ($new_form_id = !empty($form_state['values']['settings']['form_id']))) {
    $form_id = $callback = $conf['form_id'];
    if ($new_form_id) {
      $conf = $form_state['values']['settings'];
      $callback = $conf['form_id'];
    }
    $generic_form_state = FALSE;
    $generic_form = _generic_form_context_load_definition($conf, $contexts, $generic_form_state);
    if ($generic_form_state) {
      if (isset($generic_form_state['callback'])) {
        $callback = $generic_form_state['callback'];
      }
      if (function_exists($callback)) {
        // Use reflection to detect parameters.
        $reflect_callback = new ReflectionFunction($callback);
        $callback_parameters = $reflect_callback->getParameters();
        foreach (array_slice($callback_parameters, 2) as $callback_parameter) {
          $arg_name = $callback_parameter->getName();
          $default_value = NULL;
          if (!empty($conf['arguments'][$arg_name])) {
            $default_value = $conf['arguments'][$arg_name];
          }
          // @todo Add context / keyword support.
          $form['settings']['arguments'][$arg_name] = array(
            '#type' => 'textfield',
            '#title' => $arg_name,
            '#default_value' => $default_value,
          );
          if (!$callback_parameter->isDefaultValueAvailable()) {
            $form['settings']['arguments'][$arg_name]['#required'] = TRUE;
          }
        }
        if (!empty($form_state['contexts'])) {
          if (!module_exists('token')) {
            $description = t('If checked, context keywords will be substituted in the arguments. More keywords will be available if you install the Token module, see http://drupal.org/project/token.');
          }
          else {
            $description = t('If checked, context keywords will be substituted in this arguments.');
          }

          $form['settings']['substitute'] = array(
            '#type' => 'checkbox',
            '#title' => t('Use context keywords'),
            '#description' => $description,
            '#default_value' => !empty($conf['substitute']),
          );
          $form['settings']['attention'] = array(
            '#type' => 'markup',
            '#markup' => t('<strong>Attention:</strong> This feature can lead to some notices in the backend. And everytime you change the contexts this relation has to be resaved manually.'),
          );
          $form['contexts'] = array(
            '#title' => t('Substitutions'),
            '#type' => 'fieldset',
            '#collapsible' => TRUE,
            '#collapsed' => TRUE,
          );

          $rows = array();
          foreach ($contexts as $context) {
            $rows[] = array(
              check_plain('%' . check_plain($context->keyword)),
              t('@identifier: @title', array('@title' => t('Full context data'), '@identifier' => $context->identifier)),
            );
            foreach (ctools_context_get_converters('%' . check_plain($context->keyword) . ':', $context) as $keyword => $title) {
              $rows[] = array(
                check_plain($keyword),
                t('@identifier: @title', array('@title' => $title, '@identifier' => $context->identifier)),
              );
            }
          }
          $header = array(t('Keyword'), t('Value'));
          $form['contexts']['context'] = array('#markup' => theme('table', array('header' => $header, 'rows' => $rows)));
        }
      }
    }
  }

  return $form;
}

/**
 * Just return the arguments part of the form.
 */
function generic_form_settings_form_get_arguments_ajax($form, &$form_state) {
  $keywords = NULL;
  if (isset($form['settings']['substitute'])) {
    $keywords .= drupal_render($form['settings']['substitute']);
    $keywords .= drupal_render($form['contexts']);
  }
  $commands[] = ajax_command_replace(
    '.form-settings-form-arguments',
    drupal_render($form['settings']['arguments']) . $keywords
  );
  return array('#type' => 'ajax', '#commands' => $commands);
}

/**
 * Context settings form validation handler.
 */
function generic_form_settings_form_validate($form, &$form_state) {
  // Check if the file can be load if one is set.
  if (strlen(implode('', $form_state['values']['settings']['file']))) {
    $file = call_user_func_array('module_load_include', $form_state['values']['settings']['file']);
    if (!$file) {
      form_set_error('settings][file', t("The form file wasn't found. Please check the file settings"));
      // Exit, it's unlikely we can load the form if ther file isn't found.
      return FALSE;
    }
  }
  // Verify the form can be load.
  $contexts = array();
  if (!empty($form_state['contexts'])) {
    $contexts = $form_state['contexts'];
  }
  $form_definition = _generic_form_context_load_definition($form_state['values']['settings'], $contexts);
  if (!$form_definition) {
    form_set_error('settings][form_id', t("Couldn't retrive the form definition, please check the form id."));
  }
}

/**
 * Context settings form submit handler.
 */
function generic_form_settings_form_submit($form, &$form_state) {
  // Add all available contexts.
  $form_state['values']['context'] = array_keys($form_state['values']['valid_contexts']);

  // Sync settings.
  $form_state['conf'] = $form_state['values']['settings'] + $form_state['conf'];
}

/**
 * Filters the available contexts to just contains valid values.
 *
 * @param array $contexts
 *   List of available contexts.
 *
 * @return array
 *   The valid contexts.
 */
function _generic_form_prepare_contexts($contexts) {
  $valid_contexts = array();
  foreach ($contexts as $key => $context) {
    // Avoid circular dependencies.
    if ($context->plugin != 'generic_form') {
      $valid_contexts[$key] = $context;
    }
  }
  return $valid_contexts;
}

/**
 * Prepares the form state.
 *
 * Loads the include file if necessary.
 *
 * @param array $conf
 *   The configuration to load the form
 * @param array $contexts
 *   List of contexts to use for keywod substitution.
 *
 * @return array
 *   The form state to use for further form actions.
 */
function _generic_form_context_prepare_form_state($conf, $contexts = array()) {
  $form_state = array(
    'want form' => TRUE,
    'build_info' => array(
      'args' => array(),
      'requester' => 'generic_form_context',
    ),
  );

  // Check if a file needs to be load.
  if (strlen(implode('', $conf['file']))) {
    $conf['file'] = array('form_state' => &$form_state) + $conf['file'];
    $file = call_user_func_array('form_load_include', $conf['file']);
  }

  // Add arguments.
  if (!empty($conf['arguments'])) {
    if (!empty($contexts) && !empty($conf['substitute'])) {
      $context_keywords = array();
      // Prepare full context keywords.
      foreach ($contexts as $context) {
        $context_keywords['%' . check_plain($context->keyword)] = $context->data;
      }
      foreach ($conf['arguments'] as &$argument) {
        if (isset($context_keywords[$argument])) {
          // Full context data replacement.
          $argument = $context_keywords[$argument];
        }
        else {
          // Normal string replacement.
          $argument = ctools_context_keyword_substitute($argument, array(), $contexts);
        }
      }
    }
    $form_state['build_info']['args'] = $conf['arguments'];
  }

  return $form_state;
}

/**
 * Load the definition of the form.
 *
 * @param array $conf
 *   The configuration to load the form.
 * @param array $contexts
 *   List of contexts to use for keywod substitution.
 * @param NULL $form_state
 *   If defined this variable contains the form state after the form build.
 *
 * @return array|FALSE
 *   The form array or FALSE.
 */
function _generic_form_context_load_definition($conf, $contexts = array(), &$form_state = NULL) {
  $form_state = _generic_form_context_prepare_form_state($conf, $contexts);
  $form = drupal_retrieve_form($conf['form_id'], $form_state);
  if (count($form) < 2) {
    return FALSE;
  }
  return $form;
}
