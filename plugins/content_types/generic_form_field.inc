<?php

/**
 * @file
 *   Provides the buttons for the enquiry form.
 */

$plugin = array(
  'single' => TRUE,
  'icon' => 'generic_form_field.png',
  'title' => t('Generic form field'),
  'description' => t('Form field of a generic form contect'),
  'required context' => new ctools_context_required(t('Generic form'), 'generic_form'),
  'category' => t('Form'),
  'defaults' => array(
    'field' => NULL,
    'custom_selector' => NULL,
    'hide_field_title' => FALSE,
  ),
);

/**
 * Render the generic form field.
 */
function ctools_generic_form_generic_form_field_content_type_render($subtype, $conf, $args, &$context) {
  static $added_hidden_elements;
  $block = new stdClass();
  $block->delta = 'ctools_generic_form_generic_form_field';

  if (isset($context->form)) {
    $block->content = array();
    if ($conf['field'] == '___full_form') {
      if (empty($added_hidden_elements)) {
        $added_hidden_elements = TRUE;
        $block->content = drupal_render_children($context->form);
      }
    }
    else {
      // First call also adds the hiden elements required to handle the form.
      if (!isset($added_hidden_elements)) {
        $added_hidden_elements = TRUE;
        foreach (array('form_token', 'form_build_id', 'form_id', 'error') as $element) {
          if (isset($context->form[$element])) {
            $block->content[$element] = $context->form[$element];
            unset($context->form[$element]);
          }
        }
      }
      // Check if this a custom selector.
      if ($conf['field'] == '___field_selector') {
        $conf['field'] = $conf['custom_selector'];
      }
      // And now set output the field.
      $parents = explode(':', $conf['field']);
      if (($element = drupal_array_get_nested_value($context->form, $parents))) {
        $block->title = (isset($element['#title'])) ? $element['#title'] : t('Generic field !field', array('!field' => $conf['field']));
        if (!empty($conf['hide_field_title'])) {
          unset($element['#title']);
          // Some elements need a title to be displayed.
          if ($element['#type'] == 'checkbox' || $element['#type'] == 'radio') {
            $element['#title'] = '&nbsp;';
          }
        }
        $block->content[$conf['field']] = $element;

        // Mark as printed.
        $element['#printed'] = TRUE;
        drupal_array_set_nested_value($context->form, $parents, $element);
      }
    }
  }
  else {
    $block->content = t('Generic form field.');
  }
  return $block;
}

/**
 * Returns an edit form for the field configuration.
 */
function ctools_generic_form_generic_form_field_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'] + $form_state['plugin']['defaults'];

  // Load the context to use.
  $context_identifier = $form['context']['#default_value'];
  if (!empty($form_state['values']['context'])) {
    $context_identifier = $form_state['values']['context'];
  }
  if (empty($context_identifier)) {
    $context_identifier = key($form['context']['#options']);
  }
  $context = FALSE;
  if (!empty($context_identifier) && !empty($form_state['contexts'][$context_identifier])) {
    $context = $form_state['contexts'][$context_identifier];
  }

  if (!empty($context->form)) {
    $options = array(
      '___full_form' => t('Full form'),
      '___field_selector' => t('Custom selector'),
    ) + $context->data['fields'];
    // Provide a select box with all available fields.
    $form['field'] = array(
      '#type' => 'select',
      '#title' => t('Field'),
      '#options' => $options,
      '#default_value' => $conf['field'],
    );
    $form['custom_selector'] = array(
      '#type' => 'textfield',
      '#title' => t('Custom field selector'),
      '#description' => t(
        "Define your owm field selector to display conditional displayed fields. Selector are the <a href='!url'>parents</a> of the field glued by colon. The selector uses <a href='!url2'>drupal_array_get_nested_value()</a> to fetch the field/value.",
        array(
          '!url' => 'http://api.drupal.org/api/drupal/developer!topics!forms_api_reference.html/7#parents',
          '!url2' => 'http://api.drupal.org/api/drupal/includes!common.inc/function/drupal_array_get_nested_value/7',
        )
      ),
      '#default_value' => $conf['custom_selector'],
      '#states' => array(
        'visible' => array(
          'select[name="field"]' => array('value' => '___field_selector'),
        ),
      ),
    );
    $form['hide_field_title'] = array(
      '#type' => 'checkbox',
      '#title' => t('Hide title of input field'),
      '#description' => t('If enabled the title of the field is hidden and onyl visible as pane title.'),
      '#default_value' => !empty($conf['hide_field_title']),
    );
  }
  else {
    $form['error'] = array(
      '#type' => 'markup',
      '#markup' => t('No valid context found - please reconfigure context and load form again.'),
    );
  }
  return $form;
}

/**
 * Submit handler for the settings form.
 */
function ctools_generic_form_generic_form_field_content_type_edit_form_submit(&$form, &$form_state) {
  foreach (array_keys($form_state['plugin']['defaults']) as $key) {
    if (isset($form_state['values'][$key])) {
      $form_state['conf'][$key] = $form_state['values'][$key];
    }
  }
}

/**
 * Returns the administrative title for a type.
 */
function ctools_generic_form_generic_form_field_content_type_admin_title($subtype, $conf, $context) {
  $options = array(
    '___full_form' => t('Full form'),
    '___field_selector' => t('Custom selector (!selector)', array('!selector' => $conf['custom_selector'])),
  ) + $context->data['fields'];
  return t('"@s" form field "!field"', array('@s' => $context->identifier, '!field' => $options[$conf['field']]));
}
