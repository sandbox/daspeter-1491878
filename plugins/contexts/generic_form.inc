<?php

/**
 * @file
 *   Generic form context to contextifie any form.
 *
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 *
 * @todo Currently there seems to be no way to do keyword substitution using the
 *   base contexts. To achieve this we need to change some essential parts of
 *   ctools context handling.
 */
$plugin = array(
  'title' => t('Generic Form'),
  'description' => t('Allowes to use nearly any form as a ctools context.'),
  'context' => 'generic_form_create_context',
  'context name' => 'generic_form',
  'identifier' => 'generic_form',
  'keyword' => 'generic_form',
  'convert list' => 'generic_form_convert_list',
  'convert' => 'generic_form_convert',

  'defaults' => array(
    'file' => array(
      'type' => NULL,
      'module' => NULL,
      'name' => NULL,
    ),
    'form_id' => FALSE,
    'arguments' => array(),
    'substitute' => TRUE,
  ),

  'placeholder form' => array(
    '#type' => 'textfield',
    '#description' => t('Enter some data to represent this "generic_form".'),
  ),
  'no ui' => TRUE,
);

/**
 * Create the form context.
 */
function generic_form_create_context($empty, $data = NULL, $conf = FALSE, $plugin = array()) {
  static $creating = FALSE, $built_forms;
  $context = new ctools_context(array('form', 'generic_form'));
  $context->plugin = 'generic_form';

  if ($empty || $creating) {
    return $context;
  }
  $data += $plugin['defaults'];
  // Don't buil the same form more than once per request.
  $form_hash = md5($data['form_id'] . serialize($data['arguments']));
  if (isset($built_forms[$form_hash])) {
    return $built_forms[$form_hash];
  }

  // Avoid creating while creating.
  $creating = TRUE;

  // Ensure the relationship with its helper function is load.
  module_load_include('inc', 'ctools_generic_form', 'plugins/relationships/generic_form');

  // If this isn't ajax - build form. Otherwise use cached form.
  $is_form_ajax = (isset($_POST['ajax_page_state']) && isset($_POST['form_id']) && $_POST['form_id'] == $data['form_id']);
  if (!$is_form_ajax) {
    $form = _generic_form_context_load_form($data, $data['contexts']);
  }
  else {
    // Get the already prepared form.
    list($form, $form_state) = ajax_get_form();
  }

  $context->title       = t('Generic Form');
  $context->argument   = $data;
  $context->data       = $data;
  $context->form       = $form;
  $context->form_state = &$form_state;
  $context->form_id    = $data['form_id'];
  $context->form_title = $context->title;

  $context->restrictions['form'] = array('form');
  $context->data['fields']       = _generic_form_fields($form);

  $built_forms[$form_hash] = $context;
  $creating = FALSE;
  return $context;
}

/**
 * Load the form.
 *
 * @param array $conf
 *   The configuration to load the form.
 * @param array $contexts
 *   List of contexts to use for keywod substitution.
 * @param NULL $form_state
 *   If defined this variable contains the form state after the form build.
 *
 * @return array|FALSE
 *   The form array or FALSE.
 */
function _generic_form_context_load_form($conf, $contexts = array(), &$form_state = NULL) {
  $form_state = _generic_form_context_prepare_form_state($conf, $contexts);
  $form = drupal_build_form($conf['form_id'], $form_state);
  return $form;
}

/**
 * Returns an array with all the fields the form has as flat array.
 */
function _generic_form_fields($form) {
  // Always reset static var when first entering the recursion.
  drupal_static_reset('_generic_form_context_fields_flatten');
  return _generic_form_context_fields_flatten($form);
}

/**
 * Helper function for _generic_form_fields().
 *
 * Iterates over the form fields and produces a flat array that has unique
 * keys.
 *
 * @see _form_options_flatten()
 */
function _generic_form_context_fields_flatten($array) {
  $return = &drupal_static(__FUNCTION__);

  foreach (element_get_visible_children($array) as $key) {
    $element = $array[$key];
    $parents = implode(':', $element['#parents']);
    $title = (isset($element['#title'])) ? $element['#title'] . ' (' . $parents . ')' : $parents;
    $return[$parents] = $title;
    if (is_array($element)) {
      _generic_form_context_fields_flatten($element);
    }
  }
  return $return;
}

/**
 * Provide a list of sub-keywords.
 *
 * This is used to provide keywords from the context for use in a content type,
 * pane, etc.
 */
function generic_form_convert_list() {
  return array();
}

/**
 * Convert a context into a string to be used as a keyword by content types etc.
 */
function generic_form_convert($context, $type) {
  return FALSE;
}
